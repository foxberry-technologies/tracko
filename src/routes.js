import React from 'react';

const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'))

// main menu
const Role = React.lazy(() => import('./views/role/Roles'))
const Permission = React.lazy(() => import('./views/role/Permission'))
const User = React.lazy(() => import('./views/user/User'))
const Project = React.lazy(() => import('./views/projects/Project'))

//menus
const Backlog = React.lazy(() => import('./views/backlog/Backlog'))
const Board = React.lazy(() => import('./views/board/Board'))
const Reports = React.lazy(() => import('./views/Reports/Reports'))
const Releases = React.lazy(() => import('./views//releases/Releases'))
const Component = React.lazy(() => import('./views/component/Component'))
const Issues = React.lazy(() => import('./views/issues/Issues'))
const Repository = React.lazy(() => import('./views/repo/Repo'))
const Setting = React.lazy(() => import('./views/settings/Settings'))

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', element: Dashboard },
  { path: '/role', name: 'Role & Permission', element: Role },
  { path: '/role/permission', name: 'Permission', element: Permission },
  { path: '/user', name: 'User', element:User},
  { path: '/path', name: 'Project', element: Project},
  { path: '/backlog', name: 'Backlog', element: Backlog},
  { path: '/board', name: 'Board', element: Board },
  { path: '/reports', name: 'Reports', element: Reports},
  { path: '/releases', name: 'Releases', element: Releases},
  { path: '/component', name: 'Component', element: Component},
  { path: '/issues', name: 'Issues', element: Issues},
  { path: '/repo', name: 'Repository', element: Repository},
  { path: '/settings', name: 'Setting', element: Setting},
]
export default routes