import React from "react";
import { CRow,CCol, CForm,CFormCheck, CFormLabel,CFormInput, CButton,CInputGroupText,CInputGroup, CCardTitle,CFormSelect, CTable, CTableFoot,CTableRow, CTableHead,CTableBody,CTableDataCell,CTableHeaderCell } from '@coreui/react'

function Add() {
  return (
    <>
        <CRow>
        <CCol md={12}>
            <CForm className="g-3 customForm1">
                {/* <CRow className='mb-3'>
                <CCol md={12}>
                    <CFormLabel htmlFor="roleId">Role Id</CFormLabel>
                    <CFormInput type="text" id="roleId" placeholder=''/>
                </CCol>
                </CRow> */}
            
                <CRow className='mb-3'>
                <CCol xs={6}>
                    <CFormLabel htmlFor="mTitle">Name</CFormLabel>
                    <CFormInput type='text' id="name" placeholder="Enter Name"/>
                </CCol>
                <CCol xs={6}>
                    <CFormLabel htmlFor="eTitle">Email</CFormLabel>
                    <CFormInput type='email' id="email" placeholder="Enter Email"/>
                </CCol>
                </CRow>
                <CRow className='mb-3'>
                <CCol xs={6}>
                    <CFormLabel htmlFor="mobile">Moblie Number</CFormLabel>
                    <CFormInput type='number' id="mobile" placeholder="Enter Mobile"/>
                </CCol>
                <CCol xs={6}>
                    <CFormLabel htmlFor="address">Address</CFormLabel>
                    <CFormInput type='text' id="address" placeholder="Enter Address"/>
                </CCol>
                </CRow>
                {/*  */}
                
                <CRow>
                <CCol style={{marginBottom:'10px', textAlign:'right'}} xs={12}>
                    <CButton type="submit">Save</CButton>
                </CCol>
                </CRow>
            </CForm>
        </CCol>
        </CRow>
    </>
  );
}

export default Add;
