import React,{useState} from 'react'
import { CRow,CCol, CForm,CFormCheck, CFormLabel,CFormInput, CButton, CTable,CTableRow, CTableHead,CTableBody,CTableDataCell,CTableHeaderCell,CDropdown,CDropdownToggle, CDropdownItem,CDropdownDivider,CDropdownMenu } from '@coreui/react'
import CIcon from '@coreui/icons-react';
import { cilPencil, cilTrash } from '@coreui/icons';

function User() {
  
    const myComponent = {
      width: '100%',
      height: '80%',
      overflowY: 'scroll',
      overflowX: 'hidden',
      //background: 'rgba(67, 129, 168,0.5)'
  };
  return (
    <CRow>
      <CCol md={4}>
        <CForm className="g-3 customForm">
          
          <CRow className='mb-12'>
            <h1>Onbording</h1>
            <CCol xs={12} style={{marginBottom:'10px'}}>
              <CFormLabel htmlFor="roleName">Full Name</CFormLabel>
              <CFormInput type='text' id="roleName" placeholder="Enter Full Name"/>
            </CCol>
          </CRow>

          <CRow className='mb-12'>
          <CFormLabel htmlFor="roleName">Job Position</CFormLabel>
            <CCol xs={12}>
            <CDropdown variant="btn-group" xs={8}>
             {/* <CButton color="secondary" size="lg">Large split button</CButton> */}
              <CDropdownToggle color="" size="lg" style={{border:'10px',background: '#d8d6ed', width:'100%'}}>Select</CDropdownToggle>
              <CDropdownMenu>
                <CDropdownItem >Developer</CDropdownItem>
                <CDropdownItem >Tester</CDropdownItem>
                <CDropdownItem >Team Lead</CDropdownItem>
                {/* <CDropdownDivider/>
                <CDropdownItem href="#">Separated link</CDropdownItem> */}
              </CDropdownMenu>
            </CDropdown>

            <CRow className='mb-3'>
              <CCol style={{marginTop:'10px', textAlign:'right'}} xs={12}>
                <CButton type="submit">Save</CButton>
              </CCol>
            </CRow>

            </CCol>
          </CRow>
        </CForm>
      </CCol>
      
      <CCol md={8} className="xsDivMargin">
          <CTable align="middle" responsive>
            <CTableHead color="dark" className='text-center'>
              <CTableRow>
                <CTableHeaderCell scope="col" className="w-25">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col" className="w-25">Permission</CTableHeaderCell>
                <CTableHeaderCell scope="col" className="w-25">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody className='text-center'>
              <CTableRow>
                <CTableDataCell>
                  145987632
                </CTableDataCell>
                <CTableDataCell>
                  User
                </CTableDataCell>
                <CTableDataCell>
                  <CButton style={{background:'green'}}><CIcon  icon={cilPencil}/></CButton>
                  <CButton style={{background:'red', marginLeft:'5px'}}><CIcon  icon={cilTrash}/></CButton>
                </CTableDataCell>
              </CTableRow>
            </CTableBody>
          </CTable>
      </CCol>
    </CRow>
  )
}

export default User