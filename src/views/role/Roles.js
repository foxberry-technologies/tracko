import React,{useState} from 'react'
import { CRow,CCol, CForm,CFormCheck, CFormLabel,CFormInput, CButton, CCardTitle,CFormSelect, CTable, CTableFoot,CTableRow, CTableHead,CTableBody,CTableDataCell,CTableHeaderCell } from '@coreui/react'
import CIcon from '@coreui/icons-react';
import { cilPencil, cilTrash } from '@coreui/icons';

function Roles() {
  
    const myComponent = {
      width: '100%',
      height: '80%',
      overflowY: 'scroll',
      overflowX: 'hidden',
      //background: 'rgba(67, 129, 168,0.5)'
  };
  return (
    <CRow>
      <CCol md={5}>
        <CForm className="g-3 customForm">
          
          <CRow className='mb-3'>
            <CCol xs={12}>
              <CFormLabel htmlFor="roleName">Name</CFormLabel>
              <CFormInput type='text' id="roleName" placeholder="Enter Role Name"/>
            </CCol>
          </CRow>

          <CRow className='mb-3'>
            <CCol >
            {/* <CFormLabel htmlFor="roleName" className='lbl-previllage'>Permission</CFormLabel> */}

              <CCol className='scroll' style={myComponent} xs={12}>
                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Dashboard:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Setting:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Role:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Permission:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Designation:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Projects:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Task:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>User:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

                <CRow className='mb-3'>
                  <CCol sm={4}>
                    <CFormLabel>Backlog:</CFormLabel>
                  </CCol>
                  <CCol sm={8} className='d-flex'>
                    <CFormCheck inline id="flexCheckDefault" label="View"/>
                    <CFormCheck inline id="flexCheckDefault" label="Edit"/>
                    <CFormCheck inline id="flexCheckChecked" label="Delete"  />
                  </CCol>
                </CRow>

              </CCol>

              <CRow className='mb-3'>
                <CCol style={{marginTop:'10px', textAlign:'right'}} xs={12}>
                  <CButton type="submit">Save</CButton>
                </CCol>
              </CRow>

            </CCol>
          </CRow>
        </CForm>
      </CCol>
      
      <CCol md={7} className="xsDivMargin">
          <CTable align="middle" responsive>
            <CTableHead color="dark" className='text-center'>
              <CTableRow>
                <CTableHeaderCell scope="col" className="w-25">Name</CTableHeaderCell>
                <CTableHeaderCell scope="col" className="w-25">Permission</CTableHeaderCell>
                <CTableHeaderCell scope="col" className="w-25">Action</CTableHeaderCell>
              </CTableRow>
            </CTableHead>
            <CTableBody className='text-center'>
              <CTableRow>
                <CTableDataCell>
                  145987632
                </CTableDataCell>
                <CTableDataCell>
                  User
                </CTableDataCell>
                <CTableDataCell>
                  <CButton style={{background:'green'}}><CIcon  icon={cilPencil}/></CButton>
                  <CButton style={{background:'red', marginLeft:'5px'}}><CIcon  icon={cilTrash}/></CButton>
                </CTableDataCell>
              </CTableRow>
            </CTableBody>
          </CTable>
      </CCol>
    </CRow>
  )
}

export default Roles