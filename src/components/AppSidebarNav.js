import React from 'react'
import { NavLink, useLocation } from 'react-router-dom'
import PropTypes from 'prop-types'

import { CBadge,CNavItem } from '@coreui/react'

export const AppSidebarNav = ({ items }) => {
  const location = useLocation()
  console.log(items);
  
  const navLink = (name, icon, badge,item) => {
    return (
      <>
        {icon && icon}
        {name && name}
        {badge && (
          <CBadge color={badge.color} className="ms-auto">
            {badge.text}
          </CBadge>
        )}
      </>
    )
  }

  const navItem = (item, index) => {
    const { component, name, badge, icon, ...rest } = item
    const Component = component
    return (
      <CNavItem
        {...(rest.to &&
          !rest.items && {
            component: NavLink,
          })}
        key={index}
        style={ localStorage.getItem("Active") === item.to ? {
          backgroundColor:'#1c2358'
        } : null }
        {...rest}
          onClick={ () => localStorage.setItem("Active",item.to) }
      >
        {navLink(name, icon, badge,item)}
      </CNavItem>
    )
  }
  const navGroup = (item, index) => {
    const { component, name, icon, to, ...rest } = item
    const Component = component
    return (
      <Component
        idx={String(index)}
        key={index}
        toggler={navLink(name, icon)}
        visible={location.pathname.startsWith(to)}
        {...rest}
      >
        {item.items?.map((item, index) =>
          item.items ? navGroup(item, index) : navItem(item, index),
        )}
      </Component>
    )
  }

  return (
    <React.Fragment>
      
      {items &&
        items.map((item, index) => (item.items ? navGroup(item, index) : navItem(item, index)))}
    </React.Fragment>
  )
}

AppSidebarNav.propTypes = {
  items: PropTypes.arrayOf(PropTypes.any).isRequired,
}
