import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cibLogstash,
  cilBadge,
  cilBell,
  cilBoatAlt,
  cilCalculator,
  cilChartLine,
  cilChartPie,
  cilCheckCircle,
  cilCode,
  cilCursor,
  cilDescription,
  cilDrop,
  cilLineWeight,
  cilNotes,
  cilPencil,
  cilPuzzle,
  cilReportSlash,
  cilRouter,
  cilSettings,
  cilSpeedometer,
  cilStar,
  cilUser,
  cilWarning,
} from '@coreui/icons'
import { CNavGroup, CNavItem, CNavTitle } from '@coreui/react'
import { useState } from 'react'

//Toggle for navbar


const _nav = [
  {
    component: CNavItem,
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon icon={cilSpeedometer} customClassName="nav-icon" />,
    badge: {
      color: 'info',
      text: 'NEW',
    },
  },
  {
    component: CNavGroup,
    name: 'Role & Permission',
    to: '/role',
    icon: <CIcon icon={cilLineWeight} customClassName="nav-icon" />,
    items: [
        {
          component: CNavItem,
          name: 'Role',
          to: '/role',
        },
        {
          component: CNavItem,
          name: 'Permission',
          to: '/role/permission',
        },
    ]
  },
  {
    component: CNavItem,
    name: 'User',
    to: '/user',
    icon: <CIcon icon={cilUser} customClassName="nav-icon" />,
  },
  // show&&(
  {
    component: CNavItem,
    name: 'Project',
    to: '/project',
    icon: <CIcon icon={cilLineWeight} customClassName="nav-icon" />,
  },
  {
    component: CNavTitle,
    name: 'Project',
  },
  {
    component: CNavItem,
    name: 'Backlog',
    to: '/backlog',
    icon: <CIcon icon={cilLineWeight} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Board',
    to: '/board',
    icon: <CIcon icon={cibLogstash} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Reports',
    to: '/reports',
    icon: <CIcon icon={cilChartLine} customClassName="nav-icon" />,    
  },
  {
    component: CNavItem,
    name: 'Releases',
    to: '/release',
    icon: <CIcon icon={cilRouter} customClassName="nav-icon" />,    
  },
  {
    component: CNavItem,
    name: 'Component',
    to: '/component',
    icon: <CIcon icon={cilNotes} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Issues',
    to: '/issues',
    icon: <CIcon icon={cilLineWeight} customClassName="nav-icon" />,
  },
  {
    component: CNavItem,
    name: 'Repository',
    to: '/repo',
    icon: <CIcon icon={cilCode} customClassName="nav-icon" />,   
  },
  {
    component: CNavItem,
    name: 'Setting',
    to: '/settings',
    icon: <CIcon icon={cilSettings} customClassName="nav-icon" />,
    
  },
  // )
  
]

export default _nav
